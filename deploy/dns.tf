data "aws_route53_zone" "zone" {
  name = "${var.dns_zone_name}."
}

resource "aws_route53_record" "app" {
  zone_id = data.aws_route53_zone.zone.zone_id
  # retrieve the appropriate subdomain by using workspace to lookup subdomain
  name = "${lookup(var.subdomain, terraform.workspace)}.${data.aws_route53_zone.zone.name}"
  # canonical name
  type = "CNAME"
  # anytime we make a change to dns, 5 minutes for change to propagate through dns server
  ttl = "300"

  # map name to dns_name
  records = [aws_lb.api.dns_name]
}

resource "aws_acm_certificate" "cert" {
  # create certificate for https to validate domain name
  domain_name       = aws_route53_record.app.fqdn
  validation_method = "DNS"

  tags = local.common_tags

  lifecycle {
    # to prevent errors when running or destroying environment
    create_before_destroy = true
  }
}

resource "aws_route53_record" "cert_validation" {
  # validate certificate you just created
  name    = aws_acm_certificate.cert.domain_validation_options.0.resource_record_name
  type    = aws_acm_certificate.cert.domain_validation_options.0.resource_record_type
  zone_id = data.aws_route53_zone.zone.zone_id
  records = [
    aws_acm_certificate.cert.domain_validation_options.0.resource_record_value
  ]
  ttl = "60"
}

resource "aws_acm_certificate_validation" "cert" {
  # trigger validation process in aws
  certificate_arn         = aws_acm_certificate.cert.arn
  validation_record_fqdns = [aws_route53_record.cert_validation.fqdn]
}
