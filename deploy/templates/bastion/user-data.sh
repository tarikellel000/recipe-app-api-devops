#!/bin/bash

# script that is ran when you first run you bastion instance

sudo yum update -y
sudo amazon-linux-extras install -y docker
sudo systemctl enable docker.service
sudo systemctl start docker.service
sudo usermod -aG docker ec2-user 
# run and manage docker through the account