variable "prefix" {
  default = "raad" # recipe app api devops
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "email@tareklel.com"
}

variable "db_username" {
  # if variables are not defined in env, variables are asked for on deployment
  # add variables to terraform.tfvars if not in environment
  # or you can add terraform to env by adding prefix TF_VAR_
  description = "username for the rds postgres instance"
}

variable "db_password" {
  description = "Passoword for the rds postgres instance"
}

variable "bastion_key_name" {
  # key assigned on ec2 which will be rsa to connect to local machine
  default = "recipe-app-api-devops-bastion"
}


variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "106473743119.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for API"
  default     = "106473743119.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  # will be set by gitlab variable
  description = "Secret key for Django app"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "poopoop123.click"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}


