terraform {
  backend "s3" {
    bucket         = "recipe-app-api-devops-tfstate-coursework"
    key            = "recipe-app.tfstate" # key that contains terraform state
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock" # table to add lock

  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.50.0"
}

locals {
  # are a way to create dynamic variables inside terraform
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}