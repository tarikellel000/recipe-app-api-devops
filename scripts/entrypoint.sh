#!/bin/sh

set -e

# noinput supresses any question to user and assumes yes to y/n questions
python manage.py collectstatic --noinput
python manage.py wait_for_db
python manage.py migrate

# run uwsgi on port 9000, rest api doesn't require much compute so 4 workers should be fine
uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi